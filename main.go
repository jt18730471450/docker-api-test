package main

import (
	"github.com/operator-framework/operator-sdk/pkg/sdk"

	"github.com/sirupsen/logrus"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"os"
	"github.com/docker/docker/api"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/operator-framework/operator-sdk/pkg/k8sclient"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"encoding/json"
	"context"
)

var imageAddress,imageName string

const (
	dockerHostEnvVar = "DOCKER_REMOTE_HOST"
	imageNameEnvVar = "IMAGE_NAME"
)

func init(){
	imageAddress, _ = os.LookupEnv(dockerHostEnvVar)
	if len(imageAddress) == 0 {
		logrus.Errorf("%s must not be empty", dockerHostEnvVar)
	}
	logrus.Infof("DOCKER_REMOTE_HOST: %s", imageAddress)
	imageName, _ = os.LookupEnv(imageNameEnvVar)
	if len(imageName) == 0 {
		logrus.Errorf("%s must not be empty", imageNameEnvVar)
	}
	logrus.Infof("DOCKER_REMOTE_HOST: %s", imageName)
}

func createDockerClient() client.APIClient {
	var hclient *http.Client
	docker, err := client.NewClient(client.DefaultDockerHost, api.DefaultVersion, hclient, nil)
	if err != nil {
		logrus.Fatalf("Could not create a Docker client: %v", err)
	}
	return docker
}

func main() {

	sdk.ExposeMetricsPort()

	docker := createDockerClient()

	usrName := k8sclient.GetKubeConfig().Username
	token := k8sclient.GetKubeConfig().BearerToken
	logrus.Infof("usr/token/dockerhost: %s/%s/%s", usrName, token, imageAddress)

	authConfig := types.AuthConfig{
		Username:      usrName,
		Password:      token,
		ServerAddress: imageAddress,
	}
	encodedJSON, err := json.Marshal(authConfig)
	if err != nil {
		logrus.Error("json error ", err)
		return
	}
	authStr := base64.URLEncoding.EncodeToString(encodedJSON)

	imageReaderT, err := docker.ImagePull(context.Background(), imageName, types.ImagePullOptions{RegistryAuth: authStr})
	if err != nil {
		logrus.Fatalf("Could not pull Docker image [%s]: %v", imageName, err)
	}
	data, _ := ioutil.ReadAll(imageReaderT)
	logrus.Infof("-------------")
	logrus.Infof("PullImage Info: %s", string(data))
	logrus.Infof("-------------")
	imageReader, err := docker.ImageSave(context.Background(), []string{imageName})
	if err != nil {
		logrus.Fatalf("Could not save Docker image [%s]: %v", imageName, err)
	}

	_, err = docker.ImageRemove(context.Background(), imageName, types.ImageRemoveOptions{Force: true})
	if err != nil {
		logrus.Fatalf("Could not remove Docker image [%s]: %v", imageName, err)
	}

	defer imageReader.Close()

}
